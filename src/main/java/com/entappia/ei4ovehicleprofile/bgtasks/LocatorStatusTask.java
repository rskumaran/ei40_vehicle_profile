package com.entappia.ei4ovehicleprofile.bgtasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import com.entappia.ei4ovehicleprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4ovehicleprofile.models.NotificationDetailMessages;
import com.entappia.ei4ovehicleprofile.models.NotificationEvents;
import com.entappia.ei4ovehicleprofile.models.QuuppaLocator;
import com.entappia.ei4ovehicleprofile.quuppa.QuuppaApiService;
import com.entappia.ei4ovehicleprofile.udp.client.UdpClient;
import com.entappia.ei4ovehicleprofile.udp.client.UdpIntegrationClient;
import com.entappia.ei4ovehicleprofile.utils.LogEvents;
import com.entappia.ei4ovehicleprofile.utils.Utils;

@Component
public class LocatorStatusTask {

	private static ThreadPoolTaskScheduler locatorStatusTaskScheduler;
	private static boolean isRunning;

	@Autowired
	private NotificationEvents notificationEvents;

	@Autowired
	private LogEvents logEvents;

	private final UdpClient udpClient;
	
	@Autowired
	private QuuppaApiService quuppaApiService;

	@Autowired
	public LocatorStatusTask(UdpIntegrationClient udpClient) {
		this.udpClient = udpClient;
	}

	public void createSchedule() {

		if (locatorStatusTaskScheduler == null) {
			locatorStatusTaskScheduler = new ThreadPoolTaskScheduler();
			locatorStatusTaskScheduler.setPoolSize(5);
			locatorStatusTaskScheduler.initialize();
		}

		startSchedule();
	}

	public void startSchedule() {

		String cronTime = "0 0/15 * * * *";// preference.getIntValue(AppConstants.CONFIG_PREF_LS, 15)

		if (!isRunning) {
			if (locatorStatusTaskScheduler != null)
				locatorStatusTaskScheduler.schedule(new RunnableTask("PeriodicTrigger"), new CronTrigger(cronTime));

			isRunning = true;
		}
	}

	public void stopSchedule() {
		if (locatorStatusTaskScheduler != null) {
			locatorStatusTaskScheduler.getScheduledThreadPoolExecutor().shutdown();
			locatorStatusTaskScheduler.shutdown();
		}

		locatorStatusTaskScheduler = null;
		isRunning = false;
	}

	public void changeScheduleTime() {
		stopSchedule();
		createSchedule();
		startSchedule();
	}

	class RunnableTask implements Runnable {
		private String message;

		public RunnableTask(String message) {
			this.message = message;
		}

		@SuppressWarnings("unused")
		@Override
		public void run() {

			ExecutorService executorService = Executors.newFixedThreadPool(2);
	 		executorService.execute(new Runnable() {
	 			public void run() {
	 				CompletableFuture<List<QuuppaLocator>> getTagCompletableFuture;
					try {
						getTagCompletableFuture = quuppaApiService.getLocatorInfo();
						CompletableFuture.allOf(getTagCompletableFuture).join();
						 
		 				if (getTagCompletableFuture.isDone()) {
		 					checkLocatorData(getTagCompletableFuture.get());
		 				}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	 				
	 			}
	 		});
	 		
			/*
			 * ResourceBundle bundle = ResourceBundle.getBundle("application"); WebClient
			 * client = WebClient.create(bundle.getString("QUUPPA_HOST_NAME"));
			 * 
			 * Flux<QuuppaLocatorResponse> employeeFlux =
			 * client.get().uri(AppConstants.GET_LOCATOR_INFO).retrieve()
			 * .bodyToFlux(QuuppaLocatorResponse.class);
			 * 
			 * employeeFlux.subscribe(quuppaLocatorResponse -> {
			 * checkLocatorData(quuppaLocatorResponse); }); employeeFlux.doOnError(error ->
			 * { System.out.println(error.getLocalizedMessage()); });
			 */

		}
	};

	private void checkLocatorData(List<QuuppaLocator> quuppaLocatorList) {
 
				if (quuppaLocatorList != null) {

					List<HashMap<String, String>> locatorStatusList = new ArrayList<>();

					for (QuuppaLocator quuppaLocator : quuppaLocatorList) {

						String connection = quuppaLocator.getConnection();
						String id = quuppaLocator.getId();

						if (!Utils.isEmptyString(connection) && !Utils.isEmptyString(id)) {
							if (!connection.equalsIgnoreCase("ok")) {

								HashMap<String, String> map = new HashMap<>();
								map.put("name", quuppaLocator.getName());
								map.put("status", connection);
								map.put("id", id);
								locatorStatusList.add(map);
							}
						}
					}

					if (locatorStatusList.size() > 0) {
						JSONArray mJSONArray = new JSONArray(locatorStatusList);

						JSONObject jsonObject = new JSONObject();
						jsonObject.put("status", 200);
						jsonObject.put("type", "locator");
						jsonObject.put("data", mJSONArray);
						udpClient.sendMessage(jsonObject.toString());
   
						
						NotificationDetailMessages notificationDetailMessages = new NotificationDetailMessages();
						notificationDetailMessages.setType("locator");
						notificationDetailMessages.setData(locatorStatusList);
						
						notificationEvents.saveNotification(NotificationType.EMERGENCY, "Locator",
								mJSONArray.length() + " locator lost connection", notificationDetailMessages);
					}

				} 

	}
}
