package com.entappia.ei4ovehicleprofile.bgtasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import com.entappia.ei4ovehicleprofile.constants.AppConstants;
import com.entappia.ei4ovehicleprofile.datacollection.TagScheduleAsyncTask;
import com.entappia.ei4ovehicleprofile.utils.Preference;
 
 
@Component
public class TagSchedulerTask {
	private static ThreadPoolTaskScheduler tagApiTaskScheduler;
	private static boolean isRunning;
	public String TAG_NAME = "TagSchedulerTask";

	Preference preference = new Preference();
	
	@Autowired
	private TagScheduleAsyncTask tagScheduleTask;
	
	public void createSchedule() {

		if (tagApiTaskScheduler == null) {
			tagApiTaskScheduler = new ThreadPoolTaskScheduler();
			tagApiTaskScheduler.setPoolSize(5);
			tagApiTaskScheduler.initialize();
		}

		startSchedule();
	}

	public void startSchedule() {

		String cronTime = "0 0/1 * * * *";
		if (AppConstants.print_log)
			System.out.println("HelloApiTask cronTime: " + cronTime);

		if (!isRunning) {
			if (tagApiTaskScheduler != null)
				tagApiTaskScheduler.schedule(new RunnableTask(), new CronTrigger(cronTime));

			isRunning = true;
		}
	}

	public void stopSchedule() {
		if (tagApiTaskScheduler != null)
		{
			tagApiTaskScheduler.getScheduledThreadPoolExecutor().shutdown();
			tagApiTaskScheduler.shutdown();
		}

		tagApiTaskScheduler = null;
		isRunning = false;
	}

	public void changeScheduleTime() {
		stopSchedule();
		createSchedule();
		startSchedule();
	}
	
	class RunnableTask implements Runnable {

		public RunnableTask() {
		}

		@SuppressWarnings("unused")
		@Override
		public void run() {

			//tagScheduleTask.getTagPositions();
		}
	};
	
}
