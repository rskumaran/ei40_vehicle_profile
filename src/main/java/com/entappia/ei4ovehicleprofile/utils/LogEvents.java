package com.entappia.ei4ovehicleprofile.utils;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entappia.ei4ovehicleprofile.constants.AppConstants.LogEventType;
import com.entappia.ei4ovehicleprofile.dbmodels.VehicleLogs;
import com.entappia.ei4ovehicleprofile.repository.VehicleLogsRepository;

@Service
public class LogEvents {

	@Autowired
	VehicleLogsRepository vehicleLogsRepository;

	public synchronized void addLogs(LogEventType type, String major, String minor, String message) {
		if (vehicleLogsRepository != null) {
			VehicleLogs logs = new VehicleLogs();
			logs.setDate(new Date());

			if (LogEventType.ERROR == type)
				logs.setType("Error");
			else
				logs.setType("Event");
			logs.setMajor(major);
			logs.setMinor(minor);
			logs.setResult(message);
			vehicleLogsRepository.save(logs);
		}
	}

}
