package com.entappia.ei4ovehicleprofile.repository;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovehicleprofile.dbmodels.LocationHistory;
 

@Repository
public interface LocationHistoryDataRepository extends CrudRepository<LocationHistory, Integer> {

	LocationHistory findByDate(Date date);
	
}
