package com.entappia.ei4ovehicleprofile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovehicleprofile.dbmodels.Tags;

@Repository
public interface TagsRepository extends CrudRepository<Tags, Integer> {
	Tags findByTagId(String tagId);
	 
	Tags findByMacId(String macId);
	
	@Query(value = "select * from tags where mac=:mac and tag_id!=:tagid", nativeQuery = true)
	Tags checkMacId(@Param("mac") String mac,
			@Param("tagid") String tagid);
	
	List<Tags> findAll();
}
