package com.entappia.ei4ovehicleprofile.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovehicleprofile.dbmodels.TagPosition;

@Repository
public interface TagPositionRepository  extends CrudRepository<TagPosition , Integer> {
	
	List<TagPosition> findAll();

}
