package com.entappia.ei4ovehicleprofile.repository;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovehicleprofile.dbmodels.VehicleLogs;
 

@Repository
public interface VehicleLogsRepository extends CrudRepository<VehicleLogs, Integer> {

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM vehicle_logs WHERE date < :date", nativeQuery = true)
	int deleteByDate(@Param("date") Date date);

}
