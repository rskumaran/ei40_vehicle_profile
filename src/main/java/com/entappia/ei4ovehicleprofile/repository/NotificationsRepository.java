package com.entappia.ei4ovehicleprofile.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovehicleprofile.dbmodels.Notifications;

@Repository
public interface NotificationsRepository extends CrudRepository<Notifications, Integer>{

}
