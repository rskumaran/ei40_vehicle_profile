package com.entappia.ei4ovehicleprofile.quuppa;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.entappia.ei4ovehicleprofile.constants.AppConstants;
import com.entappia.ei4ovehicleprofile.models.QuuppaLocator;
import com.entappia.ei4ovehicleprofile.models.QuuppaTag;
import com.entappia.ei4ovehicleprofile.utils.RestErrorHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class QuuppaApiService {

	Gson gson = new Gson();
	
	Type tagListType = new TypeToken<List<QuuppaTag>>() {
	}.getType();
	
	Type locatorListType = new TypeToken<List<QuuppaLocator>>() {
	}.getType();

	@SuppressWarnings("unused")
	@Async
	public CompletableFuture<List<QuuppaTag>> getTagDetails() throws InterruptedException {

		String zoneUrl = getQuuppaURL(AppConstants.GET_TAG_URL);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		List<QuuppaTag> quuppaTagList = null;

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(zoneUrl, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {

				String tagData = response.getBody();
				try {
					JSONObject tagsObject = new JSONObject(tagData);
					if (tagsObject != null) {
						JSONArray tagsArray = tagsObject.optJSONArray("tags");
						if (tagsArray != null) {
							quuppaTagList = gson.fromJson(tagsArray.toString(), tagListType);
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (ResourceAccessException e) {
			e.printStackTrace();
		}

		return CompletableFuture.completedFuture(quuppaTagList);
	}

	@Async
	public CompletableFuture<List<QuuppaLocator>> getLocatorInfo() throws InterruptedException {

		String zoneUrl = getQuuppaURL(AppConstants.GET_LOCATOR_INFO);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		List<QuuppaLocator> locatorList = null;

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(zoneUrl, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {

				String locatorData = response.getBody();
				try {
					JSONObject locatorObject = new JSONObject(locatorData);
					if (locatorObject != null) {
						JSONArray locatorsArray = locatorObject.optJSONArray("locators");
						if (locatorsArray != null) {
							locatorList = gson.fromJson(locatorsArray.toString(), locatorListType);
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (ResourceAccessException e) {
			e.printStackTrace();
		}

		return CompletableFuture.completedFuture(locatorList);

	}

	@Async
	public CompletableFuture<JSONObject> getProjectInfo() throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = getQuuppaURL(AppConstants.GET_PROJECT_INFO);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		JSONObject projectInfoJsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {

				String projectInfo = response.getBody();
				JSONObject json = new JSONObject();

				try {
					json = new JSONObject(projectInfo);

					if (json != null) {

						int code = json.getInt("code");
						if (code == 0) {

							JSONArray jsonArray = json.getJSONArray("coordinateSystems");
							if (jsonArray != null && jsonArray.length() > 0) {

								JSONObject json1 = jsonArray.getJSONObject(0);
								if (json1 != null) {

									JSONArray projectInfoArray = json1.getJSONArray("backgroundImages");
									if (projectInfoArray != null && projectInfoArray.length() > 0) {
										JSONObject projectJsonObject = projectInfoArray.getJSONObject(0);
										projectInfoJsonObject.put("backgroundImages", projectJsonObject);
									}

									JSONArray locatorsArray = json1.getJSONArray("locators");
									if (locatorsArray != null && locatorsArray.length() > 0) {
										projectInfoJsonObject.put("locatorsArray", locatorsArray);

									}

									projectInfoJsonObject.put("status", "success");
								}
							}
						} else {
							String message = json.getString("message");
							projectInfoJsonObject.put("status", "error");
							projectInfoJsonObject.put("message", message);
						}
					}

				} catch (JSONException e) {
					projectInfoJsonObject.put("status", "error");
					projectInfoJsonObject.put("message", e.getMessage());
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			} else {

				projectInfoJsonObject.put("status", "error");
				projectInfoJsonObject.put("message",
						"Status Code:" + response.getStatusCodeValue() + ", Connection Failed");

			}
		} catch (ResourceAccessException e) {
			projectInfoJsonObject.put("status", "error");
			projectInfoJsonObject.put("message", e.getMessage());
		}
		return CompletableFuture.completedFuture(projectInfoJsonObject);
	}

	public String getQuuppaURL(String url) {
		ResourceBundle bundle = ResourceBundle.getBundle("application");
		return bundle.getString("QUUPPA_HOST_NAME") + url;
	}

}
