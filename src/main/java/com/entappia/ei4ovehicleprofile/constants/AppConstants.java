package com.entappia.ei4ovehicleprofile.constants;

public class AppConstants {

	public static boolean print_log = true;
	public static String profileName = "vehicleprofile";
	
	public static enum AssignmentStatus {
		NOT_ASSIGNED, ASSIGNED
	}
	
	public static enum AssignmentType {
		 PERSON, DEPT, TAG
	}
	 
	public static enum LogEventType {
		EVENT, 
		ERROR
	}
	
	public static enum ZoneType {
		Trackingzone,
		DeadZone
	}
	
	public static enum NotificationType {
		INFORMATION, WARNING, EMERGENCY
	}
	
	public static final String timeZone = "Asia/Kolkata";
	
	
	public static final String seckretKey = "Ei4.0123!!!";

	
	public static final String request = "Request";
	public static String success = "Success";
	public static String errormsg = "Error in process";
	
	public static final String session_time = "session_time";
	public static final String sessiontimeout = "Session timed-out or Invalid";
	public static final String fieldsmissing = "Mandatory fields are missing";
	
	

	// Quuppa Apis
	public static String GET_QPE_INFO = "/qpe/getPEInfo?version=2";
	public static String GET_PROJECT_INFO = "/qpe/getProjectInfo?version=2";
	public static String GET_TAG_URL = "/qpe/getTagData?version=2&maxAge=60000&radius=10";
	public static String GET_LOCATOR_INFO = "/qpe/getLocatorInfo?version=2";

}
