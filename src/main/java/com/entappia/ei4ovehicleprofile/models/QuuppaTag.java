package com.entappia.ei4ovehicleprofile.models;

import java.util.List;

public class QuuppaTag {

	String tagId;
	long locationTS;
	List<Double> location;
	
	public String getTagId() {
		return tagId;
	}
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	public long getLocationTS() {
		return locationTS;
	}
	public void setLocationTS(long locationTS) {
		this.locationTS = locationTS;
	}
	public List<Double> getLocation() {
		return location;
	}
	public void setLocation(List<Double> location) {
		this.location = location;
	}

 
}
