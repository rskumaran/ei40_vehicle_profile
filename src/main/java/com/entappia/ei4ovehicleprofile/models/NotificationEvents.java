package com.entappia.ei4ovehicleprofile.models;

import java.util.Date;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4ovehicleprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4ovehicleprofile.dbmodels.Notifications;
import com.entappia.ei4ovehicleprofile.repository.NotificationsRepository;

@Component
public class NotificationEvents {
	

	@Autowired
	private NotificationsRepository notificationRepository;
	
	public synchronized void saveNotification(NotificationType type, String source,  String message ,  NotificationDetailMessages messageObj)
	{
		
		Notifications notifications = new Notifications();
		notifications.setType(type);
		notifications.setSource(source);
		notifications.setMessage(message);
		notifications.setMessageObj(messageObj);
		notifications.setNotifyTime(new Date());
		notificationRepository.save(notifications);
	}

}
