package com.entappia.ei4ovehicleprofile.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4ovehicleprofile.udp.client.UdpClient;
import com.entappia.ei4ovehicleprofile.udp.client.UdpIntegrationClient;
 

@RestController
public class LoginRegistrationController {

	private final static Logger LOGGER = LoggerFactory.getLogger(LoginRegistrationController.class);

	private final UdpClient udpClient;

	@Autowired
	public LoginRegistrationController(UdpIntegrationClient udpClient) {
		this.udpClient = udpClient;
	}
 

	@CrossOrigin(origins = "*")
	@RequestMapping(path = { "/message" }, method = RequestMethod.POST)
	public ResponseEntity<?> test(@RequestBody HashMap<String, String> requestMap, HttpSession session)
			throws InterruptedException, ExecutionException, JSONException {
		Map<String, Object> returnmap = new HashMap<>();
		returnmap.put("status", "success");
 
		try { 

			String payload = requestMap.get("message");
			LOGGER.info("Received HTTP POST message: {}", payload);
			udpClient.sendMessage(payload);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			returnmap.put("status", "Error");
		}

		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

}
