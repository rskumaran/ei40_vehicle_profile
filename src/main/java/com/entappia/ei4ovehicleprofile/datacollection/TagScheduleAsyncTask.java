package com.entappia.ei4ovehicleprofile.datacollection;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.entappia.ei4ovehicleprofile.dbmodels.CampusDetails;
import com.entappia.ei4ovehicleprofile.dbmodels.LocationHistory;
import com.entappia.ei4ovehicleprofile.dbmodels.TagPosition;
import com.entappia.ei4ovehicleprofile.dbmodels.Tags;
import com.entappia.ei4ovehicleprofile.dbmodels.ZoneDetails;
import com.entappia.ei4ovehicleprofile.models.HistoryLocationData;
import com.entappia.ei4ovehicleprofile.models.QuuppaTag;
import com.entappia.ei4ovehicleprofile.quuppa.QuuppaApiService;
import com.entappia.ei4ovehicleprofile.repository.CampusDetailsRepository;
import com.entappia.ei4ovehicleprofile.repository.LocationHistoryDataRepository;
import com.entappia.ei4ovehicleprofile.repository.TagPositionRepository;
import com.entappia.ei4ovehicleprofile.repository.TagsRepository;
import com.entappia.ei4ovehicleprofile.repository.ZoneRepository;
import com.entappia.ei4ovehicleprofile.udp.client.UdpClient;
import com.entappia.ei4ovehicleprofile.udp.client.UdpIntegrationClient;
import com.entappia.ei4ovehicleprofile.utils.LogEvents;
import com.entappia.ei4ovehicleprofile.utils.Preference;
import com.entappia.ei4ovehicleprofile.utils.TagEvents;
import com.entappia.ei4ovehicleprofile.utils.Utils;
import com.snatik.polygon.Point;
import com.snatik.polygon.Polygon;

@Component
public class TagScheduleAsyncTask {

	public String TAG_NAME = "TagScheduleTask";

	Preference preference = new Preference();

	@Autowired
	private QuuppaApiService quuppaApiService;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private CampusDetailsRepository campusDetailsRepository;

	@Autowired
	private ZoneRepository zoneRepository;

	@Autowired
	private LogEvents logEvents;

	@Autowired
	private TagEvents tagEvents;

	@Autowired
	private TagPositionRepository tagPositionRepository;
	
	@Autowired
	private LocationHistoryDataRepository locationHistoryDataRepository;

	private final UdpClient udpClient;

	@Autowired
	public TagScheduleAsyncTask(UdpIntegrationClient udpClient) {
		this.udpClient = udpClient;
	}

	DecimalFormat df = new DecimalFormat("#.##");

	//@Scheduled(cron = "0/30 * * * * *")
	public void updateTagPositions() {
		List<TagPosition> tagPositionsList = tagPositionRepository.findAll();
		
		for(TagPosition tagPosition : tagPositionsList) {
			tagPosition.setLastSeen(System.currentTimeMillis());
		}
		
		if (tagPositionsList.size() > 0) {

			// tagPositionRepository.deleteAll();
			tagPositionRepository.saveAll(tagPositionsList);

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", 200);
			jsonObject.put("type", "tag");
			jsonObject.put("message", "Tag data read done");
			udpClient.sendMessage(jsonObject.toString());
		}
		
	}
	@Scheduled(cron = "0/10 * * * * *")
	public void getTagPositions() {

		System.out.println("Run Time:-" + Utils.getFormatedDate2(new Date()));

		ExecutorService executorService = Executors.newFixedThreadPool(2);
 		executorService.execute(new Runnable() {
 			public void run() {
 				CompletableFuture<List<QuuppaTag>> getTagCompletableFuture;
				try {
					getTagCompletableFuture = quuppaApiService.getTagDetails();
					CompletableFuture.allOf(getTagCompletableFuture).join();
					 
	 				if (getTagCompletableFuture.isDone()) {
	 					getTagData(getTagCompletableFuture.get());
	 				}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 				
 			}
 		});
 		
 		executorService.shutdown();
		/*
		 * ResourceBundle bundle = ResourceBundle.getBundle("application"); WebClient
		 * client = WebClient.create(bundle.getString("QUUPPA_HOST_NAME"));
		 * 
		 * Flux<QuuppaTagResponse> employeeFlux =
		 * client.get().uri(AppConstants.GET_TAG_URL).retrieve()
		 * .bodyToFlux(QuuppaTagResponse.class);
		 * 
		 * employeeFlux.subscribe(quuppaTagResponse -> { getTagData(quuppaTagResponse);
		 * }); employeeFlux.doOnError(error -> {
		 * System.out.println(error.getLocalizedMessage()); });
		 */
	}

	private void getTagData(List<QuuppaTag> quuppaTagList) {
 
				Calendar cal = Calendar.getInstance(); 
				int currentMins = cal.get(Calendar.MINUTE);
				 
				if (quuppaTagList != null) {

					HashMap<String, Tags> tagsMap = tagEvents.getDBTags();
					List<String> newMacIdList = new ArrayList<>();
					List<TagPosition> tagPositionsList = new ArrayList<>();

					CampusDetails campusDetails = getCampusDetails();
					HashMap<String, List<String>> gridZoneMap = new HashMap<>();
					if (campusDetails != null)
						gridZoneMap = campusDetails.getGridZoneDetails();

					double originX = campusDetails.getOriginX() * campusDetails.getMetersPerPixelX();
					double originY = campusDetails.getOriginY() * campusDetails.getMetersPerPixelY();

					for (QuuppaTag quuppaTag : quuppaTagList) {
						
						String id = quuppaTag.getTagId();
						List<Double> smoothedPosition = quuppaTag.getLocation();

						if (smoothedPosition != null) {

							double x = smoothedPosition.get(0);
							double y = smoothedPosition.get(1);

							x = Math.abs(originX - x);
							y = Math.abs(originY - y);

							TagPosition tagPosition = new TagPosition();
							tagPosition.setX(Double.valueOf(df.format(x)));
							tagPosition.setY(Double.valueOf(df.format(y)));
							tagPosition.setLastSeen(quuppaTag.getLocationTS());
 
							 
							if (campusDetails != null) {
								ZoneDetails zoneDetails = getZoneDetails(gridZoneMap, x, y);
								
								if (zoneDetails != null) {
									tagPosition.setZoneId(zoneDetails.getId());
									tagPosition.setZoneName(zoneDetails.getName());  
								} else {
									tagPosition.setZoneId(0);
									tagPosition.setZoneName("");
								}
							} else {
								tagPosition.setZoneId(0);
								tagPosition.setZoneName("");
							}

							tagPosition.setMacId(id);
							// tagPositionsMap.put(id, tagPosition);

							if (tagsMap == null || !tagsMap.containsKey(id)) {
								newMacIdList.add(id);
								tagPosition.setType("Free");
								tagPosition.setSubType("");
								tagPosition.setTagId(id);
								tagPosition.setAssignmentId("");
							} else {
								Tags tags = tagsMap.get(id);

								if (tags != null) {
									if (tags.getStatus().equalsIgnoreCase("Assigned")) {
										if (tags.getType() != null
												&& (tags.getType().equalsIgnoreCase("Asset NonChrono")
														|| tags.getType().equalsIgnoreCase("Asset Chrono"))) {
											tagPosition.setType(tags.getSubType() != null ? tags.getSubType() : "");
										} else
											tagPosition.setType(tags.getType() != null ? tags.getType() : "");

									} else {
										tagPosition.setType(tags.getStatus());
									}

									tagPosition.setSubType(tags.getSubType() != null ? tags.getSubType() : "");
									tagPosition.setTagId(tags.getTagId() != null ? tags.getTagId() : "");
									tagPosition.setAssignmentId(tags.getAssignmentId() != null ? tags.getAssignmentId() : "");

								} else {
									tagPosition.setType("Free");
									tagPosition.setSubType("");
									tagPosition.setTagId(id);
									tagPosition.setAssignmentId("");
								}

							}

							tagPositionsList.add(tagPosition);
						}
					}

					if (tagPositionsList.size() > 0) {

						// tagPositionRepository.deleteAll();
						tagPositionRepository.saveAll(tagPositionsList);
						
						if(currentMins%5==0) {
							String sTime = Utils.getTime(cal.getTime());
							Date date = Utils.getDate(Utils.getDate(cal.getTime()));
							LocationHistory locationHistory = locationHistoryDataRepository.findByDate(date);
							if(locationHistory==null)
							{
								locationHistory = new LocationHistory();
								locationHistory.setDate(date);
							}
							
							HashMap<String, HashMap<String, HistoryLocationData>> locationDataMap = locationHistory.getLocationData();
							if(locationDataMap==null)
							{
								locationDataMap = new HashMap<>();
								locationHistory.setLocationData(locationDataMap);
							}
							
							HashMap<String, HistoryLocationData> historyLocationMap = new HashMap<>();
							for(TagPosition tagPosition : tagPositionsList) {
								if(tagPosition!=null)
								{
									HistoryLocationData historyLocationData = new HistoryLocationData();
									historyLocationData.setX(tagPosition.getX());
									historyLocationData.setY(tagPosition.getY());
									historyLocationData.setZoneId(tagPosition.getZoneId());
									historyLocationData.setType(tagPosition.getType());
									historyLocationData.setSubType(tagPosition.getSubType());
									historyLocationData.setAssignmentId(tagPosition.getAssignmentId());
									historyLocationMap.put(tagPosition.getMacId(), historyLocationData);
								}
								
							}
							
							locationDataMap.put(sTime, historyLocationMap);
							
							locationHistoryDataRepository.save(locationHistory);
						}

						JSONObject jsonObject = new JSONObject();
						jsonObject.put("status", 200);
						jsonObject.put("type", "tag");
						jsonObject.put("message", "Tag data read done");
						udpClient.sendMessage(jsonObject.toString());
					}

					// String jsonString = gson.toJson(tagPositionsList);

					if (newMacIdList != null && newMacIdList.size() > 0) {
						tagEvents.updateTags(newMacIdList);
					}
				}  

	}

//	private void getTagData1() {
//
//		ExecutorService executorService = Executors.newFixedThreadPool(2);
//		executorService.execute(new Runnable() {
//			public void run() {
//
//				try {
//					HashMap<String, Tags> tagsMap = tagEvents.getDBTags();
//
//					List<String> newMacIdList = new ArrayList<>();
//
//					List<TagPosition> tagPositionsList = new ArrayList<>();
//
//					CompletableFuture<JSONObject> getTagCompletableFuture = quuppaApiService.getTagDetails();
//					CompletableFuture.allOf(getTagCompletableFuture).join();
//
//					if (getTagCompletableFuture.isDone()) {
//						preference.setLongValue("lastscan", System.currentTimeMillis());
//
//						JSONObject jsonObject = getTagCompletableFuture.get();
//						if (jsonObject != null) {
//
//							String status = jsonObject.optString("status");
//
//							if (!Utils.isEmptyString(status)) {
//								if (status.equals("success")) {
//
//									JSONObject jsonTagObject = jsonObject.optJSONObject("response");
//									if (jsonTagObject != null) {
//										int code = jsonTagObject.getInt("code");
//										if (code == 0) {
//											JSONArray tagsJsonArray = jsonTagObject.getJSONArray("tags");
//											if (tagsJsonArray != null) {
//
//												CampusDetails campusDetails = getCampusDetails();
//												HashMap<String, List<String>> gridZoneMap = new HashMap<>();
//												if (campusDetails != null)
//													gridZoneMap = campusDetails.getGridZoneDetails();
//
//												double originX = campusDetails.getOriginX()
//														* campusDetails.getMetersPerPixelX();
//												double originY = campusDetails.getOriginY()
//														* campusDetails.getMetersPerPixelY();
//
//												for (int i = 0; i < tagsJsonArray.length(); i++) {
//													JSONObject tagJsonObject = tagsJsonArray.getJSONObject(i);
//													String id = tagJsonObject.optString("id", "");
//													JSONArray smoothedPosition = tagJsonObject
//															.optJSONArray("smoothedPosition");
//
//													if (smoothedPosition != null) {
//
//														double x = smoothedPosition.getDouble(0);
//														double y = smoothedPosition.getDouble(1);
//
//														x = Math.abs(originX - x);
//														y = Math.abs(originY - y);
//
//														TagPosition tagPosition = new TagPosition();
//														tagPosition.setX(Double.valueOf(df.format(x)));
//														tagPosition.setY(Double.valueOf(df.format(y)));
//														tagPosition.setLastSeen(tagJsonObject.getLong("positionTS"));
//
//														if (campusDetails != null) {
//															ZoneDetails zoneDetails = getZoneDetails(gridZoneMap, x, y);
//															if (zoneDetails != null) {
//																tagPosition.setZoneId(zoneDetails.getId());
//																tagPosition.setZoneName(zoneDetails.getName());
//															} else {
//																tagPosition.setZoneId(0);
//																tagPosition.setZoneName("");
//															}
//														} else {
//															tagPosition.setZoneId(0);
//															tagPosition.setZoneName("");
//														}
//
//														tagPosition.setMacId(id);
//														// tagPositionsMap.put(id, tagPosition);
//
//														if (tagsMap == null || !tagsMap.containsKey(id)) {
//															newMacIdList.add(id);
//															tagPosition.setType("");
//															tagPosition.setSubType("");
//															tagPosition.setName("");
//														} else {
//															Tags tags = tagsMap.get(id);
//															if (tags != null) {
//																tagPosition.setType(
//																		tags.getType() != null ? tags.getType() : "");
//																tagPosition.setSubType(
//																		tags.getSubType() != null ? tags.getSubType()
//																				: "");
//																tagPosition.setName(
//																		tags.getTagId() != null ? tags.getTagId() : "");
//															} else {
//																tagPosition.setType("");
//																tagPosition.setSubType("");
//																tagPosition.setName("");
//															}
//
//														}
//
//														tagPosition.setType("Visitor");
//														tagPositionsList.add(tagPosition);
//													}
//												}
//											}
//										}
//
//										// logEvents.addLogs(LogEventType.EVENT, "GetQuuppaTag", "Get
//										// QuuppaTag-Schedule", "Success");
//
//									}
//								}
//
//							} else if (status.equals("error")) {
//								String message = jsonObject.getString("message");
//								logEvents.addLogs(LogEventType.ERROR, "GetQuuppaTag", "Get QuuppaTag-Schedule",
//										message);
//
//							}
//						}
//
//					}
//
//					if (tagPositionsList.size() > 0) {
//
//						// tagPositionRepository.deleteAll();
//						tagPositionRepository.saveAll(tagPositionsList);
//
//						JSONObject jsonObject = new JSONObject();
//						jsonObject.put("status", 200);
//						jsonObject.put("message", "Tag data read done");
//						udpClient.sendMessage(jsonObject.toString());
//					}
//
//					// String jsonString = gson.toJson(tagPositionsList);
//
//					if (newMacIdList != null && newMacIdList.size() > 0) {
//						tagEvents.updateTags(newMacIdList);
//					}
//				} catch (Exception e) {
//					logEvents.addLogs(LogEventType.ERROR, "GetQuuppaTag", "Get QuuppaTag-Schedule", e.getMessage());
//					e.printStackTrace();
//				}
//
//			}
//		});
//		executorService.shutdown();
//	}

	private ZoneDetails getZoneDetails(HashMap<String, List<String>> gridZoneMap, double x, double y) {

		int xValue = getGridPosition(x);
		int yValue = getGridPosition(y);

		if (gridZoneMap.containsKey(xValue + "_" + yValue)) {

			List<String> zoneIdList = gridZoneMap.get(xValue + "_" + yValue);
			if (zoneIdList != null && zoneIdList.size() > 0) {
				if (zoneIdList.size() == 1) {
					String zoneId = zoneIdList.get(0);
					Optional<ZoneDetails> zoneDetails = zoneRepository.findById(Integer.valueOf(zoneId));
					if (zoneDetails != null && zoneDetails.isPresent())
						return zoneDetails.get();
				} else {
					for (String zoneId : zoneIdList) {
						Optional<ZoneDetails> zoneDetails = zoneRepository.findById(Integer.valueOf(zoneId));
						if (zoneDetails != null && zoneDetails.isPresent()) {

							ZoneDetails zoneDetails1 = zoneDetails.get();
							List<HashMap<String, String>> coordinateList = zoneDetails1.getCoordinateList();
							if (isInsidePolygon(coordinateList, x, y))
								return zoneDetails1;
						}
					}
				}
			}
		}

		return null;
	}

	private boolean isInsidePolygon(List<HashMap<String, String>> coordinateList, double x, double y) {
		Polygon.Builder polygonBuilder = Polygon.Builder();

		for (int j = 0; j < coordinateList.size(); j++) {
			HashMap<String, String> coordinate = coordinateList.get(j);

			double x1 = Double.valueOf(coordinate.get("x"));
			double y1 = Double.valueOf(coordinate.get("y"));

			polygonBuilder.addVertex(new Point(x1, y1));

		}

		polygonBuilder.close();

		Polygon polygon = polygonBuilder.build();
		Point point = new Point(x, y);
		boolean contains = polygon.contains(point);
		if (contains) {
			return true;
		} else
			return false;
	}

	private int getGridPosition(double num1) {
		int value = 0;
		if (num1 % 1 == 0)
			value = (int) Math.floor(num1);
		else
			value = (int) Math.floor(num1 + 1);

		return value - 1;
	}

	public CampusDetails getCampusDetails() {
		return campusDetailsRepository.findByCampusId(1);
	}

	public HashMap<String, List<String>> getCampusGridList() {
		HashMap<String, List<String>> gridZoneMap = new HashMap<>();
		CampusDetails campusDetails = campusDetailsRepository.findByCampusId(1);
		if (campusDetails != null)
			gridZoneMap = campusDetails.getGridZoneDetails();

		return gridZoneMap;
	}
}
