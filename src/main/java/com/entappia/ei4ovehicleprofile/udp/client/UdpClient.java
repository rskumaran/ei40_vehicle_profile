package com.entappia.ei4ovehicleprofile.udp.client;

public interface UdpClient {
	public void sendMessage(String message);
}
